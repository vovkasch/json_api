require "#{__dir__}/../test_helper"

module JsonApi
  class Response::BuilderTest < ActiveSupport::TestCase
    test 'it builds successful response' do
      successful_response = OpenStruct.new successful?: true
      builder = JsonApi::Response::Builder.new successful_response

      result = builder.build

      assert_equal Response::Successful, result
    end

    test 'it builds erroneous response' do
      successful_response = OpenStruct.new successful?: false
      builder = JsonApi::Response::Builder.new successful_response

      result = builder.build

      assert_equal Response::Erroneous, result
    end
  end
end

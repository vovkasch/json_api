require "#{__dir__}/../test_helper"

module JsonApi
  class Response::PlainTest < ActiveSupport::TestCase
    test 'it recognizes successful response' do
      response = Response::Plain.new '{ "result" : "some data here", "some_key" : "some value" }'

      assert_equal true, response.successful?
    end
  end
end

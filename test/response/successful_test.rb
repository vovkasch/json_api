require '#{__dir__}/../test_helper'

module JsonApi
  class Response::SuccessfulTest < ActiveSupport::TestCase
    test 'it gets `token` attribute from response' do
      response = OpenStruct.new data: {'result' => {'token' => 'a token'}}
      request = OpenStruct.new body: '{"params" : ""}'

      result = Response::Successful.new response, request

      assert_equal 'a token', result.token
    end

    test 'it gets `token` attribute from request' do
      response = OpenStruct.new data: {'result' => {}}
      request = OpenStruct.new body: '{"params" : [{"token" : "a token"}]}'

      result = Response::Successful.new response, request

      assert_equal 'a token', result.token
    end

    test 'it gets `user_id` attribute from response' do
      response = OpenStruct.new data: {'result' => {'idUser' => 'user id'}}
      request = OpenStruct.new body: '{"params" : ""}'

      result = Response::Successful.new response, request

      assert_equal 'user id', result.user_id
    end

    test 'it gets `user_id` attribute from request' do
      response = OpenStruct.new data: {'result' => {}}
      request = OpenStruct.new body: '{"params" : [{"idUser" : "user id"}]}'

      result = Response::Successful.new response, request

      assert_equal 'user id', result.user_id
    end

    test 'it gets `account_id` attribute from response' do
      response = OpenStruct.new data: {'result' => {'idAccount' => 'account id'}}
      request = OpenStruct.new body: '{"params" : ""}'

      result = Response::Successful.new response, request

      assert_equal 'account id', result.account_id
    end

    test 'it gets `account_id` attribute from request' do
      response = OpenStruct.new data: {'result' => {}}
      request = OpenStruct.new body: '{"params" : [{"idAccount" : "account id"}]}'

      result = Response::Successful.new response, request

      assert_equal 'account id', result.account_id
    end

    test 'it gets `request_id` attribute from response' do
      response = OpenStruct.new data: {'id' => 'request id'}
      request = OpenStruct.new body: '{"params" : ""}'

      result = Response::Successful.new response, request

      assert_equal 'request id', result.request_id
    end
  end
end

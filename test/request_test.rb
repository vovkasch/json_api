require "#{__dir__}/test_helper"

module JsonApi
  module NetMock
    class HTTP
      attr_accessor :use_ssl,
                    :body # => TODO: review if it really needs `body` attribute or this attribute should be present at `start` result object

      def initialize(hostname, port) end

      def request(*args)
        if args.empty?
          @request
        else
          @request = args[0]
        end
      end

      def start(&block)
        yield self
        self # => # TODO: review what class exactly should it return
      end
    end

    class HTTP::Post
      attr_accessor :body, :uri, :header

      def initialize(uri, header)
        @uri, @header = uri, header
      end
    end
  end

  class ResponseMock
    def initialize(net_http_response, request) end
  end

  class ResponseBuilderMock
    def initialize(plain_response_instance) end

    def build
      ResponseMock
    end
  end

  class PlainResponseMock
    def initialize(net_http_response) end
  end





  class RequestTest < ActiveSupport::TestCase
    test 'it converts data to json' do
      request = Request.new net_http_class: NetMock::HTTP,
                            response_builder: ResponseBuilderMock,
                            plain_response_class: PlainResponseMock
      data = { some: 'data' }

      net_http_response = request.send data

      assert_equal '{"some":"data"}', net_http_response.request.body
    end

    test 'it sets request' do
      request = Request.new net_http_class: NetMock::HTTP,
                            response_builder: ResponseBuilderMock,
                            plain_response_class: PlainResponseMock

      net_http_response = request.send

      assert_kind_of NetMock::HTTP::Post, net_http_response.request
    end

    test 'it sets URI for request' do
      request = Request.new endpoint: 'http://example.com',
                            net_http_class: NetMock::HTTP,
                            response_builder: ResponseBuilderMock,
                            plain_response_class: PlainResponseMock

      net_http_response = request.send

      assert_equal URI('http://example.com'), net_http_response.request.uri
    end

    test 'it uses SSL' do
      request = Request.new endpoint: 'https://example.com',
                            net_http_class: NetMock::HTTP,
                            response_builder: ResponseBuilderMock,
                            plain_response_class: PlainResponseMock

      net_http_response = request.send

      assert_equal true, net_http_response.use_ssl
    end

    test 'it builds response' do
      request = Request.new net_http_class: NetMock::HTTP,
                            response_builder: ResponseBuilderMock,
                            plain_response_class: PlainResponseMock
      request.send

      result = request.response

      assert_kind_of ResponseMock, result
    end
  end
end

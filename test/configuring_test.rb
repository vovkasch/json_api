require "#{__dir__}/test_helper"

module JsonApi
  class ConfiguringTest < ActiveSupport::TestCase
    test 'it sets email option' do
      JsonApi.email = 'test@example.com'

      assert_equal 'test@example.com', JsonApi.email
    end

    test 'it sets password option' do
      JsonApi.password = '123456'

      assert_equal '123456', JsonApi.password
    end

    test 'it sets ssl option' do
      JsonApi.ssl = true

      assert_equal true, JsonApi.ssl
    end


    test 'it sets log option' do
      JsonApi.log = true

      assert_equal true, JsonApi.log
    end


    test 'it sets test option' do
      JsonApi.test = { currency_id: 1,
                       category_id: 2,
                       account_id: 3 }

     assert_equal 1, JsonApi.test[:currency_id]
     assert_equal 2, JsonApi.test[:category_id]
     assert_equal 3, JsonApi.test[:account_id]
   end

   test 'endpoint value depends on configuration. # TODO: is not implemented yet. ' do
     JsonApi.ssl = true

     assert_match /^https/, JsonApi::ENDPOINT

     JsonApi.ssl = false

     assert_match /^http[^s]/, JsonApi::ENDPOINT
   end
  end
end

require 'net/http'

module JsonApi
  class Request
    attr_reader :uri, :request, :response_builder, :http, :plain_response_class
    attr_accessor :net_http_response

    delegate :hostname, to: :uri
    delegate :port, to: :uri

    private :uri, :request, :response_builder, :http, :net_http_response,
            :net_http_response=, :hostname, :port, :plain_response_class

    HEADER = {'Content-Type' => 'application/json'}

    def initialize(endpoint: JsonApi::ENDPOINT,
                   response_builder: JsonApi::Response::Builder,
                   net_http_class: Net::HTTP,
                   plain_response_class: JsonApi::Response::Plain)
      @uri = URI endpoint
      @response_builder = response_builder
      @plain_response_class = plain_response_class

      @request = net_http_class::Post.new uri, HEADER

      @http = net_http_class.new(hostname, port)
      @http.use_ssl = use_ssl?
      # @http.set_debug_output $stderr
    end

    def send(data = {})
      request.body = data.to_json
      log_data
      self.net_http_response = http.start { |_http| _http.request request }
    end

    def response
      response = plain_response_class.new net_http_response.body
      response_class = response_builder.new(response).build
      response_class.new response, request
    end

  private

    def use_ssl?
      uri.scheme == 'https'
    end

    def log_data
      log(request.body, __FILE__, __LINE__) if JsonApi.log
    end

    def log(data, file, line)
      p "#{file}:#{line}: #{data}"
    end
  end
end

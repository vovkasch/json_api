module JsonApi
  class Response::Builder
    attr_reader :response
    private :response

    def initialize(response)
      @response = response
    end

    def build
      if response.successful?
        Response::Successful
      else
        Response::Erroneous
      end
    end
  end
end

module JsonApi
  class Response::Successful
    attr_reader :response_data, :request_body#:token, :user_id, :account_id, :request_id
    private :response_data, :request_body

    def initialize(response, request)
      @response_data = symbolize_keys response.data
      @request_body = symbolize_keys JSON.parse(request.body)
    end

    def token
      @token ||= response_data[:result]['token'] || request_body[:params][0]['token']
    end

    def user_id
      @user_id ||= response_data[:result]['idUser'] || request_body[:params][0]['idUser']
    end

    def account_id
      @account_id ||= response_data[:result]['idAccount'] || request_body[:params][0]['idAccount']
    end

    def request_id
      @request_id ||= response_data[:id]
    end

  private

    def symbolize_keys(hash)
      OpenStruct.new(hash).to_h
    end
  end
end

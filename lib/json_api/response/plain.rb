module JsonApi
  class Response::Plain
    attr_reader :result, :error, :id, :data
    private :result, :error, :id

    def initialize(response_body)
      @data = JSON.parse response_body
      log_data
      @result = data['result']
    end

    def successful?
      result.present?
    end

  private

    def log_data
      log(data, __FILE__, __LINE__) if JsonApi.log
    end

    def log(data, file, line)
      p "#{file}:#{line}: #{data}"
    end
  end
end

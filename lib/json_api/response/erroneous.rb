module JsonApi
  class Response::Erroneous
    def initialize(response, request)
      @request = request
      @data = response.data
    end
  end
end

require 'active_support/core_ext' # for cattr_accessor
require 'yaml'

module JsonApi
  autoload :Request, 'json_api/request'
  autoload :Response, 'json_api/response'

  cattr_accessor :email, :password, :ssl, :log, :test

  ENDPOINT = "http#{'s' if ssl}://home.finance.ua/services/index.php"
end
